from collections import OrderedDict

from PyQt5.QtCore import *
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QLabel, QComboBox, QPushButton, \
    QLineEdit, QMessageBox, QListView, QDialog, QGridLayout, QAbstractItemView, QFrame, QColorDialog

from logic import VoronoiDiagram


class AddPointWindow(QDialog):
    def __init__(self, parent=None):
        super(AddPointWindow, self).__init__(parent)
        self.resize(300, 200)

        self.voronoi_points = parent.view_of_map.voronoi_diagram
        self.x = QLineEdit()
        self.y = QLineEdit()

        self.combo_box = QComboBox()
        self.combo_box.addItem('Punkt kontrolny')
        self.combo_box.addItems(t.ob_type for t in self.voronoi_points.definitions)

        self.point_properties_layout = None
        self.list_of_properties_widgets = []
        self.button = QPushButton('Dodaj punkt')

        self.main_layout = QVBoxLayout()
        self.init_window(0)

    def init_window(self, index):
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.combo_box.setEditable(True)
        self.combo_box.lineEdit().setAlignment(Qt.AlignCenter)
        self.combo_box.lineEdit().setReadOnly(True)
        self.combo_box.currentIndexChanged.connect(self.on_combobox_changed)
        self.combo_box.setCurrentIndex(index)
        self.main_layout.addWidget(self.combo_box, alignment=Qt.AlignCenter)

        self.x.setPlaceholderText('x')
        self.x.setAlignment(Qt.AlignCenter)
        self.y.setPlaceholderText('y')
        self.y.setAlignment(Qt.AlignCenter)

        xy_hbox_layout = QHBoxLayout()
        xy_hbox_layout.setSpacing(10)

        xy_hbox_layout.addWidget(self.x)
        xy_hbox_layout.addWidget(self.y)
        self.main_layout.addItem(xy_hbox_layout)

        self.button.clicked.connect(self.button_clicked)
        self.main_layout.addWidget(self.button)

        self.setWindowTitle('Dodaj punkt')
        self.setLayout(self.main_layout)
        self.layout().update()

    def rebuild_window(self, point_type):
        if self.point_properties_layout is not None:
            self.point_properties_layout.deleteLater()

        if not point_type.__eq__('Punkt kontrolny'):
            self.point_properties_layout = QGridLayout()
            definition = self.voronoi_points.find_proper_definition(point_type)

            for key in iter(definition.atr):
                label = QLabel(key)
                label.setAlignment(Qt.AlignRight)
                line_edit = QLineEdit()
                line_edit.setPlaceholderText(definition.atr[key])
                line_edit.setAlignment(Qt.AlignLeft)

                self.point_properties_layout.addWidget(label, len(self.list_of_properties_widgets), 0)
                self.point_properties_layout.addWidget(line_edit, len(self.list_of_properties_widgets), 1)
                self.list_of_properties_widgets.append(label)
                self.list_of_properties_widgets.append(line_edit)

            self.main_layout.addLayout(self.point_properties_layout)

        else:
            self.point_properties_layout = None

        self.button.deleteLater()
        self.button = QPushButton("Dodaj punkt")
        self.button.clicked.connect(self.button_clicked)
        self.main_layout.addWidget(self.button, alignment=Qt.AlignBottom)

    def on_combobox_changed(self):
        for widget in self.list_of_properties_widgets:
            widget.deleteLater()

        point_type = self.combo_box.currentText()
        self.list_of_properties_widgets = []
        self.rebuild_window(point_type)
        self.main_layout.update()

        self.parent().refresh()

    def button_clicked(self):
        point_type = self.combo_box.currentText()

        point = VoronoiDiagram.ObjectPoint(0, 0, point_type)
        point = validate_xy(voronoi_diagram=self.voronoi_points, x=self.x, y=self.y, point=point)

        if point is None:
            return

        if not self.voronoi_points.if_point_exists(VoronoiDiagram.Point(point.x, point.y)):

            if point_type == 'Punkt kontrolny':
                self.voronoi_points.control_points.append(VoronoiDiagram.ControlPoint(point.x, point.y))
                self.parent().refresh()

            else:
                params = OrderedDict()
                ob_point = VoronoiDiagram.ObjectPoint(point.x, point.y, point_type)

                for object_property in self.list_of_properties_widgets:
                    if type(object_property) is QLabel:
                        var_name = object_property.text()
                        continue
                    if self.validate_param(object_property.placeholderText(), object_property.text()) is False:
                        show_message(additional_info="Błędne dane.")
                        return

                    params[var_name] = object_property.text()

                ob_point.parameters = params

                cp = self.voronoi_points.find_nearest_control_point(point.x, point.y)
                if cp is not None:
                    ob_point.parent = cp
                    cp.objects.append(ob_point)
                    self.voronoi_points.objects.append(ob_point)

                    self.parent().refresh()
                else:
                    show_message(message="Brak punktów kontrolnych",
                                 additional_info="Należy zdefiniować najpierw punkt kontrolny.")

    def validate_param(self, p_type, p_data):
        if p_type.lower() == 'string':
            if len(p_data) > 50:
                show_message(message="Napisy mogą mieć maksymalnie 50 znaków.")
                return False
            return True

        elif p_type.lower() == 'int':
            try:
                new_int = int(p_data)

                if new_int < 0:
                    return False
                elif new_int > 1000000:
                    return False

            except ValueError:
                return False

            return True

        elif p_type.lower() == 'double':
            try:
                new_double = float(p_data)

                if new_double < 0:
                    return False
                elif new_double > 1000000:
                    return False

            except ValueError:
                return False

            return True

        return False


class StatsWindow(QDialog):
    def __init__(self, parent=None, pos=None):
        super(StatsWindow, self).__init__(parent)
        self.setMinimumSize(300, 200)

        self.main_layout = QVBoxLayout()
        self.voronoi_diagram = parent.view_of_map.voronoi_diagram

        self.combo_box = QComboBox()
        self.populace = QLabel()
        self.number_of_objects = QLabel()
        self.object_types = QListView()

        check_if_init_with_point(self, pos)

    def init_window(self, index):
        self.main_layout.setSpacing(20)
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.combo_box.setEditable(True)
        self.combo_box.lineEdit().setAlignment(Qt.AlignCenter)
        self.combo_box.lineEdit().setReadOnly(True)
        self.combo_box.addItems([i.name for i in self.voronoi_diagram.control_points])
        self.combo_box.setCurrentIndex(index)
        self.combo_box.currentIndexChanged.connect(self.on_combobox_changed)

        self.main_layout.addWidget(self.combo_box, alignment=Qt.AlignCenter)

        populace_hbox_layout = QHBoxLayout()
        populace_hbox_layout.setSpacing(10)
        populace_label = QLabel('POPULACJA')
        self.populace.setText(str(self.voronoi_diagram.control_points[index].populace))
        populace_hbox_layout.addWidget(populace_label, alignment=Qt.AlignRight)
        populace_hbox_layout.addWidget(self.populace, alignment=Qt.AlignLeft)
        self.main_layout.addItem(populace_hbox_layout)

        number_hbox_layout = QHBoxLayout()
        number_hbox_layout.setSpacing(10)
        number_label = QLabel('LICZBA OBIEKTÓW')
        self.number_of_objects.setText(str(len(self.voronoi_diagram.control_points[index].objects)))
        number_hbox_layout.addWidget(number_label, alignment=Qt.AlignRight)
        number_hbox_layout.addWidget(self.number_of_objects, alignment=Qt.AlignLeft)
        self.main_layout.addItem(number_hbox_layout)

        self.build_object_types_list(index)
        self.main_layout.addWidget(self.object_types, alignment=Qt.AlignCenter)

        self.setLayout(self.main_layout)
        self.setWindowTitle('Statystyki')

    def on_combobox_changed(self, index):
        self.populace.setText(str(self.voronoi_diagram.control_points[index].populace))
        self.number_of_objects.setText(str(len(self.voronoi_diagram.control_points[index].objects)))
        self.build_object_types_list(index)

        self.update()

    def build_object_types_list(self, index):
        model = QStandardItemModel()
        for i in self.voronoi_diagram.control_points[index].object_types:
            model.appendRow(set_item_for_list(i))

        self.object_types.setModel(model)


class ModifyPointWindow(QDialog):
    def __init__(self, parent=None, pos=None):
        super(ModifyPointWindow, self).__init__(parent)
        self.setMinimumSize(400, 200)

        self.main_layout = QHBoxLayout()
        self.voronoi_diagram = parent.view_of_map.voronoi_diagram
        self.list_of_objects = QListView()
        self.x = QLineEdit()
        self.y = QLineEdit()
        self.modify_button = QPushButton("Modyfikuj")
        self.delete_button = QPushButton("Usuń")
        if pos is not None:
            self.init_window(self.voronoi_diagram.find_point(VoronoiDiagram.Point(pos.x(), pos.y())))
        else:
            self.init_window()

    def init_window(self, chosen_point=None):
        self.main_layout.setSpacing(20)
        self.main_layout.setAlignment(Qt.AlignCenter)
        self.build_points_list()
        self.list_of_objects.clicked.connect(self.on_list_clicked)
        self.list_of_objects.setSelectionMode(QAbstractItemView.SingleSelection)
        self.main_layout.addWidget(self.list_of_objects)

        if chosen_point is not None:
            self.x.setText(chosen_point.x)
            self.y.setText(chosen_point.y)

        grid_layout = QGridLayout()
        grid_layout.setSpacing(10)
        x_label = QLabel('X')
        x_label.setAlignment(Qt.AlignRight)
        grid_layout.addWidget(x_label, 0, 0)
        grid_layout.addWidget(self.x, 0, 1)

        y_label = QLabel('Y')
        y_label.setAlignment(Qt.AlignRight)
        grid_layout.addWidget(y_label, 1, 0)
        grid_layout.addWidget(self.y, 1, 1)

        self.modify_button.clicked.connect(self.on_modify_button_clicked)
        grid_layout.addWidget(self.modify_button, 3, 0)
        self.delete_button.clicked.connect(self.on_delete_button_clicked)
        grid_layout.addWidget(self.delete_button, 3, 1)

        self.main_layout.addItem(grid_layout)
        self.setLayout(self.main_layout)
        self.setWindowTitle('Edytuj punkt')

    def on_list_clicked(self, index):
        point = self.list_of_objects.model().itemFromIndex(index).point_object
        self.x.setText(str(point.x))
        self.y.setText(str(point.y))

        self.update()

    def on_modify_button_clicked(self):
        if len(self.list_of_objects.selectedIndexes()) == 0:
            show_message(additional_info="Brak punktów do zmodyfikowania.")
            return

        index = self.list_of_objects.selectedIndexes()[0]
        point = self.list_of_objects.model().itemFromIndex(index).point_object

        point = validate_xy(voronoi_diagram=self.voronoi_diagram, x=self.x, y=self.y, point=point)
        if point is None:
            return

        if point is VoronoiDiagram.ObjectPoint:
            point.parent.objects.remove(point)
            cp = self.voronoi_diagram.find_nearest_control_point(point.x, point.y)

            cp.objects.append(point)
            point.parent = cp

        self.list_of_objects.model().itemFromIndex(index).setText(point.__str__())
        self.parent().refresh()
        self.update()

        show_message(additional_info="Punkt został zmodyfikowany.")

    def on_delete_button_clicked(self):
        if len(self.list_of_objects.selectedIndexes()) == 0:
            show_message(additional_info="Brak punktów do usunięcia.")
            return
        index = self.list_of_objects.selectedIndexes()[0]
        point = self.list_of_objects.model().itemFromIndex(index).point_object

        for cp in self.voronoi_diagram.control_points:
            if point in self.voronoi_diagram.control_points:
                objects = point.objects
                self.voronoi_diagram.control_points.remove(point)
                for o in objects:
                    nearest_cp = self.voronoi_diagram.find_nearest_control_point(o.x, o.y)
                    if nearest_cp is not None:
                        nearest_cp.objects.append(o)
                break

            elif point in cp.objects:
                cp.objects.remove(point)
                self.voronoi_diagram.objects.remove(point)

        self.list_of_objects.model().removeRow(index.row())

        if len(self.list_of_objects.selectedIndexes()) != 0:
            self.on_list_clicked(self.list_of_objects.selectedIndexes()[0])
        else:
            self.x.setText('')
            self.y.setText('')

        self.parent().refresh()
        self.update()

    def build_points_list(self):
        model = QStandardItemModel()

        for cp in self.voronoi_diagram.control_points:
            for o in cp.objects:
                model.appendRow(set_item_for_list(o))

        for i in self.voronoi_diagram.control_points:
            model.appendRow(set_item_for_list(i))

        self.list_of_objects.setModel(model)


class ModifyColorWindow(QDialog):
    def __init__(self, parent=None, pos=None):
        super(ModifyColorWindow, self).__init__(parent)
        self.setMinimumSize(300, 200)
        self.voronoi_diagram = parent.view_of_map.voronoi_diagram

        self.main_layout = QVBoxLayout()
        self.combo_box = QComboBox()
        self.color = None
        self.label = QLabel(' ')
        self.pick_button = QPushButton('Wybierz')

        if pos is not None:
            self.init_window(self.voronoi_diagram.find_nearest_control_point(pos.x(), pos.y()))
        else:
            self.init_window()

    def init_window(self, chosen_point=None):
        self.main_layout.setSpacing(20)
        self.main_layout.setAlignment(Qt.AlignCenter)

        if chosen_point is None:
            chosen_point = self.voronoi_diagram.control_points[0]

        self.combo_box.setEditable(True)
        self.combo_box.lineEdit().setAlignment(Qt.AlignCenter)
        self.combo_box.lineEdit().setReadOnly(True)
        self.combo_box.addItems([i.name for i in self.voronoi_diagram.control_points])
        self.combo_box.currentIndexChanged.connect(self.on_combobox_changed)
        self.combo_box.setCurrentIndex(self.voronoi_diagram.control_points.index(chosen_point))
        self.main_layout.addWidget(self.combo_box, alignment=Qt.AlignCenter)

        hboxlayout = QHBoxLayout()
        hboxlayout.setSpacing(10)

        self.label.setFrameShape(QFrame.StyledPanel)
        self.color = 'rgb(' + str(chosen_point.color[0]) + ', ' + str(chosen_point.color[1]) + ', ' + str(
            chosen_point.color[2]) + ')'
        self.label.setStyleSheet('background-color: ' + self.color)
        hboxlayout.addWidget(self.label, alignment=Qt.AlignCenter)
        self.pick_button.clicked.connect(self.on_pick_button_click)
        hboxlayout.addWidget(self.pick_button, alignment=Qt.AlignCenter)

        self.main_layout.addItem(hboxlayout)

        self.setLayout(self.main_layout)
        self.setWindowTitle('Modyfikuj kolor')

    def on_combobox_changed(self, index):
        chosen_point = self.voronoi_diagram.control_points[index]
        self.color = 'rgb(' + str(chosen_point.color[0]) + ', ' + str(chosen_point.color[1]) + ', ' + str(
            chosen_point.color[2]) + ')'
        self.label.setStyleSheet('background-color: ' + self.color)

        self.update()

    def on_pick_button_click(self):
        self.color = QColorDialog().getColor().toRgb()
        color = 'rgb(' + str(self.color.red()) + ', ' + str(self.color.green()) + ', ' + str(self.color.blue()) + ')'

        cp = self.voronoi_diagram.control_points[self.combo_box.currentIndex()]
        cp.color = [self.color.red(), self.color.green(), self.color.blue()]

        self.label.setStyleSheet('background-color: ' + color)
        self.parent().refresh()
        self.update()


def set_item_for_list(element):
    item = QStandardItem(element.__str__())
    item.point_object = element
    item.setEditable(False)
    item.setTextAlignment(Qt.AlignCenter)
    return item


def check_if_init_with_point(dialog, pos):
    if pos is not None:
        nearest_control_point = dialog.voronoi_diagram.find_nearest_control_point(pos.x(), pos.y())
        dialog.init_window(dialog.voronoi_diagram.control_points.index(nearest_control_point))
    else:
        dialog.init_window(0)


def validate_xy(x, y, voronoi_diagram, point=None):
    if len(x.text()) == 0 or len(y.text()) == 0:
        show_message(additional_info="Brak współrzędnych.")
        return
    try:
        new_x = round(float(x.text().replace(',', '.')), 2)

        new_y = round(float(y.text().replace(',', '.')), 2)

        if new_x < 0 or new_x > 1_000 or new_y < 0 or new_y > 1_000:
            raise ValueError

        if VoronoiDiagram.Point(new_x, new_y).__eq__(point):
            raise Warning

    except ValueError:
        show_message(message="Niepoprawne dane",
                     additional_info="Współrzędna musi być dodatnią liczbą niewiększą niż 1000 z maksymalnie 2 miejscami po przecinku.",
                     icon=QMessageBox.Warning)
        return None

    except Warning:
        show_message(additional_info="Współrzędne nie zostały zmienione.", icon=QMessageBox.Warning)
        return None

    else:
        if voronoi_diagram.if_point_exists(VoronoiDiagram.Point(new_x, new_y)):
            return None

        point.x = new_x
        point.y = new_y
        return point


def show_message(message_title=None, message=None, additional_info=None, icon=QMessageBox.Information):
    msg = QMessageBox()
    msg.setTextFormat(Qt.RichText)
    msg.setStyleSheet('QMessageBox {text-align: center;}')
    msg.setStandardButtons(QMessageBox.Ok)

    msg.setWindowTitle(message_title)
    msg.setText(message)
    msg.setInformativeText(additional_info)
    msg.setIcon(icon)

    msg.exec_()
