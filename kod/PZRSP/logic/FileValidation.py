from logic import FunctionalWindows, VoronoiDiagram
import collections
import math


class ObjectDefinition:
    def __init__(self, ob_type):
        self.ob_type = ob_type
        self.atr = collections.OrderedDict()


class FileValidation:

    def __init__(self, parent=None):
        self.parent = parent
        self.contour_points = []
        self.control_points = []
        self.objects_definitions = []
        self.objects = []

    def is_file_valid(self, file_name):
        file = open(file_name, 'r', encoding="utf8")
        file_lines = file.readlines()

        if len(file_lines) is 1:
            FunctionalWindows.show_message(
                message="Plik jest pusty.",
                additional_info="Załaduj inny plik.")
            return

        for fl in file_lines:
            fl = fl.replace(',', '.')
            fl = fl.upper()

        i = 1

        while file_lines[i][0] != '#':
            self.check_contour_points(file_lines, i)
            i = i + 1
        i = i + 1

        while file_lines[i][0] != '#':
            self.check_control_points(file_lines, i)
            i = i + 1
        i = i + 1

        while file_lines[i][0] != '#':
            self.check_definitions(file_lines, i)
            i = i + 1
        i = i + 1

        while i < len(file_lines):
            self.check_objects(file_lines, i)
            i = i + 1

        file.close()

    def check_contour_points(self, file_lines, i):
        if len(file_lines[i]) > 5:
            file_lines[i] = file_lines[i].replace("\n", '')
            elements = file_lines[i].split(' ')

            p = VoronoiDiagram.Point(round(float(elements[1].replace(",", ".")), 2), round(float(elements[2].replace(",", ".")), 2))
            if len(self.contour_points) >= 200:
                FunctionalWindows.show_message(
                    message="Punkt konturu (" + elements[1] + "," + elements[2] + ") nie  może zostać dodany.",
                    additional_info="Została już umieszczona maksymalna liczba 200 punktów konturu. "
                                    "Punkt zostanie pominięty.")
                return

            if float(elements[1].replace(",", ".")) > 500 or float(elements[2].replace(",", ".")) > 500:
                FunctionalWindows.show_message(
                    message="Punkt konturu (" + elements[1] + "," + elements[2] + ") nie  może zostać dodany.",
                    additional_info="Punkty muszą mieć współrzędne z zakresu [0,500].")
                return

            self.contour_points.append(p)

    def check_control_points(self, file_lines, i):
        if len(file_lines[i]) > 7:
            file_lines[i] = file_lines[i].replace("\n", '')
            elements = file_lines[i].split(' ')
            name = ''
            for j in range(3, len(elements)):
                name = name + elements[j] + ' '

            name = name.replace("\n", '')

            if len(self.control_points) >= 200:
                FunctionalWindows.show_message(
                    message="Punkt kontrolny (" + elements[1] + "," + elements[2] + ") nie  może zostać dodany.",
                    additional_info="Została już umieszczona maksymalna liczba 200 punktów kontrolnych."
                                    "Punkt zostanie pominięty.")
                return

            if float(elements[1].replace(",", ".")) > 500 or float(elements[2].replace(",", ".")) > 500:
                FunctionalWindows.show_message(
                    message="Punkt kontrolny (" + elements[1] + "," + elements[2] + ") nie  może zostać dodany.",
                    additional_info="Punkty muszą mieć współrzędne z zakresu [0,500].")
                return

            p = VoronoiDiagram.ControlPoint(round(float(elements[1].replace(",", ".")), 2), round(float(elements[2].replace(",", ".")), 2))
            p.name = name
            p.parametrs = []
            self.control_points.append(p)

    def check_definitions(self, file_lines, i):
        if len(file_lines[i]) > 21:
            file_lines[i] = file_lines[i].replace("\n", '')
            elements = file_lines[i].split(' ')
            ob_def = ObjectDefinition(elements[1])

            for j in range(2, len(elements), 2):
                ob_def.atr[elements[j]] = elements[j + 1]

            self.objects_definitions.append(ob_def)

    def check_objects(self, file_lines, i):
        if len(file_lines[i]) > 7:
            file_lines[i] = file_lines[i].replace("\n", '')
            elements = file_lines[i].split(' ')
            def_found = False

            for defs in self.objects_definitions:
                if elements[1] == defs.ob_type:
                    name = elements[1]
                    parametrs = []
                    x = 0
                    y = 0
                    def_found = True
                    ob_def = defs

            if def_found is False:
                FunctionalWindows.show_message(
                    message="Obiekt " + elements[1] + " nie może zostać dodany.",
                    additional_info="Nie ma takiej definicji obiektu.")
                return

            if len(self.objects) >= 1000:
                FunctionalWindows.show_message(
                    message="Obiekt " + elements[1] + " nie może zostać dodany.",
                    additional_info="Została już umieszczona maksymalna liczba 1000 obiektów. "
                                    "Punkt zostanie pominięty.")
                return

            if x > 500 or y > 500:
                FunctionalWindows.show_message(
                    message="Obiekt " + elements[1] + " nie może zostać dodany.",
                    additional_info="Punkty muszą mieć współrzędne z zakresu [0,500].")
                return

            k = 0
            for j in range(2, len(elements)):
                if list(ob_def.atr)[k] is 'X':
                    x = round(float(elements[j].replace(",", ".")), 2)
                elif list(ob_def.atr)[k] is 'Y':
                    y = round(float(elements[j].replace(",", ".")), 2)
                else:
                    parametrs.append(elements[j])
                k = k + 1

        obj = VoronoiDiagram.ObjectPoint(x, y, name, parametrs)
        self.objects.append(obj)


def count_distance(x1, y1, x2, y2):
    return math.sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))
