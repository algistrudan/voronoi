from math import sqrt, inf
from random import randint
import logic.FileValidation as fv
from logic import FunctionalWindows


class Voronoi:
    def __init__(self):
        self.contour_points = []
        self.control_points = []
        self.definitions = []
        self.objects = []
        self.file_validator = fv.FileValidation(parent=self)

    def reset_diagram(self, full=False):
        self.contour_points = []
        self.control_points = []
        self.objects = []
        self.file_validator = fv.FileValidation(parent=self)

        if full:
            self.definitions = []

    def find_proper_definition(self, type_name):
        for d in self.definitions:
            if d.ob_type == type_name:
                return d

    def add_points_from_file(self):
        self.contour_points = self.file_validator.contour_points
        self.control_points = self.file_validator.control_points
        for d in self.file_validator.objects_definitions:
            if 'X'in d.atr.keys():
                del d.atr['X']
            if 'Y' in d.atr.keys():
                del d.atr['Y']
        self.definitions = self.file_validator.objects_definitions
        self.objects = self.file_validator.objects

        for o in self.objects:
            cp = self.find_nearest_control_point(o.x, o.y)
            cp.objects.append(o)
            cp.object_types.add(o.ob_type)
            o.parent = cp

    def find_nearest_control_point(self, x, y):
        shortest_distance = inf
        nearest_cp = None
        for p in self.control_points:
            distance = sqrt(((p.x - x) ** 2 + (p.y - y) ** 2))

            if distance < shortest_distance:
                nearest_cp = p
                shortest_distance = distance

        return nearest_cp

    def if_point_exists(self, point):
        x = round(point.x, 2)
        y = round(point.y, 2)

        if self.find_point(Point(x, y)) is not None:
            FunctionalWindows.show_message(message="Punkt nie może zostać umieszczony we wskazanych współrzędnych.",
                                           additional_info="W tym miejscu istnieje inny punkt, podane współrzędne "
                                                           "wychodzą poza wyznaczony kontur lub punkt pokrywa się z "
                                                           "krawędzią mapy.")
            return True

        return False

    def find_point(self, point):
        for cp in self.control_points:
            if cp.__eq__(point):
                return cp

            for o in cp.objects:
                if o.__eq__(point):
                    return o

        return None


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.name = ''

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __str__(self):
        if self.name.__contains__(str(self.x) and str(self.y)):
            return self.name
        elif self.name.__contains__('('):
            elms = self.name.split('(')
            self.name = elms[0] + ' (' + str(self.x) + ', ' + str(self.y) + ')'
            return self.name
        else:
            return self.name + ' (' + str(self.x) + ', ' + str(self.y) + ')'


class ControlPoint(Point):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.name = 'punkt kontrolny (' + str(x) + ', ' + str(y) + ')'
        self.populace = 0
        self.object_types = set()
        self.objects = []
        self.color = [randint(210, 255), randint(210, 255), randint(210, 255)]


class ObjectPoint(Point):
    def __init__(self, x, y, ob_type, parameters=None, parent=None):
        super().__init__(x, y)
        self.ob_type = ob_type
        self.parameters = parameters
        self.parent = parent

    def __str__(self):
        if self.ob_type.__contains__(str(self.x) and str(self.y)):
            return self.ob_type
        elif self.ob_type.__contains__('('):
            elms = self.ob_type.split('(')
            self.ob_type = elms[0] + ' (' + str(self.x) + ', ' + str(self.y) + ')'
            return self.ob_type
        else:
            return self.ob_type + ' (' + str(self.x) + ', ' + str(self.y) + ')'

