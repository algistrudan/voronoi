from PyQt5.QtCore import *
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtWidgets import QWidget, QMenu

from logic.VoronoiDiagram import Voronoi, ControlPoint, Point


class PointMap(QWidget):
    def __init__(self, parent=None):
        super(PointMap, self).__init__()
        self.parent = parent
        self.setMouseTracking(True)
        self.resolution = 5
        self.setMinimumSize(400, 400)
        self.voronoi_diagram = Voronoi()
        self.painter = QPainter()

    def paintEvent(self, event):
        self.painter.begin(self)
        self.paintingActive()
        self.painter.setRenderHint(QPainter.Antialiasing, True)
        if len(self.voronoi_diagram.control_points) and len(self.voronoi_diagram.control_points) >= 2:
            for x in range(1, self.width(), self.resolution):
                for y in range(1, self.height(), self.resolution):
                    nearest_cp = self.voronoi_diagram.find_nearest_control_point(x, y)

                    pen = QPen(QColor(nearest_cp.color[0], nearest_cp.color[1], nearest_cp.color[2], 255))
                    pen.setWidth(self.resolution)
                    self.painter.setPen(pen)
                    self.painter.drawPoint(QPoint(x, y))

        pen = QPen(QColor(0, 0, 0, 255))
        pen.setWidth(5)
        pen.setCapStyle(Qt.RoundCap)
        self.painter.setPen(pen)

        for p in self.voronoi_diagram.control_points:
            self.painter.drawPoint(QPoint(p.x, p.y))
            self.painter.drawEllipse(QPoint(p.x, p.y), 1, 1)

        for o in self.voronoi_diagram.objects:
            self.painter.drawEllipse(QPoint(o.x, o.y), 0.5, 0.5)

        self.painter.end()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.add_point_interactive(event)

    def contextMenuEvent(self, event):
        menu = QMenu(self)

        add_point_action = menu.addAction("Dodaj punkt")
        show_stats_action = menu.addAction("Pokaż statystyki")
        modify_color_action = menu.addAction("Modyfikuj kolor obszaru")

        action = menu.exec_(self.mapToGlobal(event.pos()))

        if action == add_point_action:
            self.parent.add_new_point(point_pos=event.pos())
        elif action == show_stats_action:
            self.parent.show_stats(point_pos=event.pos())
        elif action == modify_color_action:
            self.parent.modify_color(point_pos=event.pos())

    def add_point_interactive(self, event):
        x = round(event.x(), 2)
        y = round(event.y(), 2)

        if self.voronoi_diagram.if_point_exists(Point(x, y)):
            return

        point = ControlPoint(event.x(), event.y())
        self.voronoi_diagram.control_points.append(point)
        self.parent.refresh()
