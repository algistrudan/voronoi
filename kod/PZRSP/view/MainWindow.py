import sys

from PyQt5 import QtWidgets
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QWidget, QApplication, QGridLayout, QMainWindow, QListView, QAction, QFileDialog, \
    QMessageBox

import view.ViewOfMap as dd
from logic import FunctionalWindows
from logic.FunctionalWindows import AddPointWindow, StatsWindow, ModifyPointWindow, ModifyColorWindow, pyqtSlot


class Gui(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.view_of_map = dd.PointMap(parent=self)
        self.menu_bar = self.menuBar()
        self.status_bar = self.statusBar()

        self.view_of_areas = QListView()
        self.model_for_areas = None
        self.view_of_objects = QListView()

        self.setUnifiedTitleAndToolBarOnMac(True)
        self.init_ui()
        self.setMinimumSize(400, 400)

    def init_ui(self):
        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        layout = QGridLayout()
        layout.setSpacing(1)
        layout.setRowStretch(1, 2)

        self.build_menu_bar()
        self.import_area_list()
        self.make_objects_list()

        layout.addWidget(self.view_of_map, 2, 0, 4, 4)
        self.view_of_areas.setWindowTitle("Punkty kontrolne")
        layout.addWidget(self.view_of_areas, 1, 0, 1, 2)
        layout.addWidget(self.view_of_objects, 1, 2, 1, 2)

        central_widget.setLayout(layout)
        self.resize(600, 600)
        self.setWindowTitle('PZRSP')
        self.show()

    def build_menu_bar(self):
        file_menu = self.menu_bar.addMenu('Plik')

        add_new_file_action = QAction(' &Dodaj plik wejściowy', self)
        add_new_file_action.setStatusTip("Ta opcja pozwala załadować nowy plik.")
        add_new_file_action.triggered.connect(self.add_new_file)
        reset_map_action = QAction(' &Resetuj mapę', self)
        reset_map_action.setStatusTip("Ta opcja pozwala zresetować widok mapy. Usuwa punkty, ale zostawia definicje obiektów.")
        reset_map_action.triggered.connect(self.reset_map)
        full_reset_action = QAction(' &Resetuj wszystkie dane', self)
        full_reset_action.setStatusTip("Ta opcja pozwala zresetować widok mapy. Usuwa punkty wraz z definicjami obiektów.")
        full_reset_action.triggered.connect(lambda: self.reset_map(full=True))
        file_menu.addAction(add_new_file_action)
        file_menu.addAction(reset_map_action)
        file_menu.addAction(full_reset_action)

        edit_menu = self.menu_bar.addMenu('Edytuj')
        add_point_action = QAction(' &Dodaj punkt', self)
        add_point_action.setStatusTip("Ta opcja pozwala dodać punkt kontrolny bądź jeden ze zdefiniowanych typów z plików.")
        add_point_action.triggered.connect(lambda: self.add_new_point(point_pos=None))
        modify_point_action = QAction(' &Modyfikuj lub usuń punkt', self)
        modify_point_action.triggered.connect(lambda: self.modify_point())
        modify_point_action.setStatusTip("Ta opcja pozwala na modyfikację lub usunięcie każdego dodanego punktu lub obiektu.")

        edit_menu.addAction(add_point_action)
        edit_menu.addAction(modify_point_action)

        view_menu = self.menu_bar.addMenu('Widok')
        modify_colors_action = QAction(' &Modyfikuj kolory', self)
        modify_colors_action.triggered.connect(lambda: self.modify_color(point_pos=None))
        modify_colors_action.setStatusTip("Ta opcja pozwala na modyfikację kolorów wybranego obszaru.")
        show_stats_action = QAction(' &Pokaż statystyki', self)
        show_stats_action.triggered.connect(lambda: self.show_stats(point_pos=None))
        show_stats_action.setStatusTip("Ta opcja pozwala zobaczyć statystyki dla wybranego obszaru.")

        view_menu.addAction(modify_colors_action)
        view_menu.addAction(show_stats_action)

        help_menu = self.menu_bar.addMenu('Pomoc')
        tutorial_action = QAction(' &Instrukcja obsługi', self)
        tutorial_action.triggered.connect(lambda: self.instruction_window())
        tutorial_action.setStatusTip("Ta opcja pozwala zobaczyć krótką instrukcję obsługi.")
        about_action = QAction(' &O programie', self)
        about_action.triggered.connect(lambda: self.about_window())
        about_action.setStatusTip("Ta opcja pozwala zobaczyć informacje o programie.")

        help_menu.addAction(tutorial_action)
        help_menu.addAction(about_action)

    def import_area_list(self):
        control_points = self.view_of_map.voronoi_diagram.control_points
        self.model_for_areas = QStandardItemModel()

        if len(control_points):
            for cp in control_points:
                item = QStandardItem(cp.name)
                item.point = cp
                item.setEditable(False)
                item.setCheckState(True)
                item.setCheckable(True)
                self.model_for_areas.appendRow(item)

        self.view_of_areas.setModel(self.model_for_areas)
        self.model_for_areas.itemChanged.connect(self.on_item_changed)

    def on_item_changed(self, item):
        self.make_objects_list()

    def make_objects_list(self):
        model = QStandardItemModel()

        for index in range(self.model_for_areas.rowCount()):
            item = self.model_for_areas.item(index)
            if item.checkState() == 1 or item.checkState() == 2:
                for o in item.point.objects:
                    model.appendRow(FunctionalWindows.set_item_for_list(o))

        self.view_of_objects.setModel(model)

    def add_new_file(self):
        dialog = QFileDialog()
        options = dialog.Options()
        file_name, _ = dialog.getOpenFileName(None, "Wybierz plik", "", "Plik tekstowy (*.txt)", options=options)
        if len(file_name) is 0:
            FunctionalWindows.show_message(message='Nie wybrano pliku', icon=QMessageBox.Warning)
            return
        self.view_of_map.voronoi_diagram.file_validator.is_file_valid(file_name)
        self.view_of_map.voronoi_diagram.add_points_from_file()
        self.refresh()

    def reset_map(self, full=False):
        if full is False:
            self.view_of_map.voronoi_diagram.reset_diagram()
        else:
            self.view_of_map.voronoi_diagram.reset_diagram(full=True)

        self.refresh()

    def add_new_point(self, point_pos=None):
        add_point_window = AddPointWindow(parent=self)

        if point_pos is not None:
            add_point_window.x.setText(str(round(point_pos.x(), 2)))
            add_point_window.y.setText(str(round(point_pos.y(), 2)))

        add_point_window.exec_()

    def modify_point(self):
        if len(self.view_of_map.voronoi_diagram.control_points) == 0:
            FunctionalWindows.show_message(additional_info="Brak punktów.")
            return

        modify_point_window = ModifyPointWindow(parent=self)

        modify_point_window.x.setText('')
        modify_point_window.y.setText('')

        modify_point_window.exec_()

    def modify_color(self, point_pos=None):
        if len(self.view_of_map.voronoi_diagram.control_points) == 0:
            FunctionalWindows.show_message(additional_info="Nie ma żadnego obszaru do modyfikacji.")
            return
        modify_color_window = ModifyColorWindow(parent=self, pos=point_pos)
        modify_color_window.exec_()

    def show_stats(self, point_pos=None):
        if len(self.view_of_map.voronoi_diagram.control_points) == 0:
            FunctionalWindows.show_message(additional_info="Brak statystyk.")
            return
        stats_window = StatsWindow(parent=self, pos=point_pos)
        stats_window.exec_()

    def refresh(self):
        self.import_area_list()
        self.make_objects_list()
        self.update()

    def instruction_window(self):
        help_title = "Instrukcja obsługi"
        help_text = ("Program służy do dzielenia obszaru na podobszary zawierające 1 punkt kontrolny na podstawie "
                     "danych dostarczonych w załadowanym pliku tekstowym lub interaktywnie. Plik powinien być stworzony"
                     " zgodnie z wymaganiami opisanymi szczegółowo w załączonej specyfikacji funkcjonalnej.")
        FunctionalWindows.show_message(message_title=help_title, additional_info=help_text)

    def about_window(self):
        about_title = "O programie"
        about_text = ("Autorzy:\n       Patrycja Szczepaniak\n       Anna Pręgowska \nCzas tworzenia projektu:\n       "
                      "grudzień 2018 - styczeń 2019 \nWersja: v1.0")
        FunctionalWindows.show_message(message_title=about_title, additional_info=about_text)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    desktop = QtWidgets.QApplication.desktop()
    resolution = desktop.availableGeometry()
    gui = Gui()
    gui.move(resolution.center() - gui.rect().center())

    sys.exit(app.exec_())
