Program do wyznaczania optymalnych granic dla punktów kontrolnych.

Aplikacja wykonuje analizę zadanych punktów kontrolnych i optymalnie dzieli obszar na mniejsze części,
z których każda zawiera dokładnie jeden taki punkt oraz dodatkowo dowolną liczbę innych (zdefiniowanych przez użytkownika) obiektów. 
Wizualizacja danych jest interaktywna, co pozwala korzystającemu na modyfikacje niektórych informacji dotyczących wybranego obszaru.